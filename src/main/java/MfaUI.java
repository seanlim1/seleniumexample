
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MfaUI extends WebUI {
    private static String URL = "https://www1.mfa.gov.sg/";

    public MfaUI() {
        setURL(URL);
    }

    public void setUp() {
        openPage();
        (new WebDriverWait(getDriver(), 10)).
                until(ExpectedConditions.presenceOfElementLocated(By.className("site-menu-wrapper")));
//        System.out.println("Title: " + getDriver().getTitle());
    }

    public void enterClimateChange() {
        WebElement tabElement = getDriver().findElement(
                By.xpath("/html/body/div[1]/div/div[2]/header/div[1]/nav/div/ul/li[2]"));
        Actions action = new Actions(getDriver());
        action.moveToElement(tabElement).build().perform();

        WebElement linkElement = tabElement.findElement(By.xpath("div/div/div[3]/ul/li[1]"));
        (new WebDriverWait(getDriver(), 10)).
                until(ExpectedConditions.visibilityOf(linkElement));
        linkElement.click();
    }

    public static void main(String[] args) {

        MfaUI mfaUI = new MfaUI();

        mfaUI.setUp();

        mfaUI.enterClimateChange();

        System.out.println(mfaUI.getDriver().getCurrentUrl());

        mfaUI.closePage();
    }

}
