import org.openqa.selenium.WebDriver;

public class WebUI {
    private WebDriver webDriver;
    private String URL;

    public WebUI() {
        webDriver = DriverProvider.startDriver();
    }

    public void setURL(String url) {
        this.URL = url;
    }

    public WebDriver getDriver() {
        return webDriver;
    }

    public void openPage() {
        webDriver.get(URL);
    }

    public void closePage() {
        webDriver.quit();
    }
}
