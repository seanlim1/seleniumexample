import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.concurrent.TimeUnit;

public class DriverProvider {

    public static boolean HEADLESS_MODE = true;
    public static WebDriver webDriver;

    public static WebDriver startDriver(){
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver");
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--window-size=1200,900");
        chromeOptions.addArguments("--no-sandbox");
        if (HEADLESS_MODE) {
            chromeOptions.addArguments("--headless");
        }

        webDriver = new ChromeDriver(chromeOptions);
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        return webDriver;
    }

}
