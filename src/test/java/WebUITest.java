import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class WebUITest {

    private static WebUI webUI;

    @Before
    public void setUp() throws Exception {
        webUI = new WebUI();
        webUI.setURL("http://www.google.com");
    }

    @After
    public void tearDown() throws Exception {
        webUI.closePage();
    }

    @Test
    public void openPage() {
        webUI.openPage();
        assertEquals("Google", webUI.getDriver().getTitle());
    }

    @Test
    public void closePage() {
        webUI.closePage();
        assertTrue(webUI.getDriver().toString().contains("null"));
    }
}