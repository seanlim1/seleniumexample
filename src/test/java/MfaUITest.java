import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MfaUITest extends WebUI {

    private static MfaUI mfaUI;

    @Before
    public void setUp() throws Exception {
        mfaUI = new MfaUI();
        mfaUI.setUp();
    }

    @After
    public void tearDown() throws Exception {
        mfaUI.closePage();
    }

    @Test
    public void enterClimateChange() {
        mfaUI.enterClimateChange();
        assertTrue(mfaUI.getDriver().getCurrentUrl().toLowerCase().contains("climate-change"));
    }
}
